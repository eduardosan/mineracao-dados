\documentclass{beamer}
\usepackage[brazil]{babel}
\usepackage[T1]{fontenc} %caracteres acentuados
\usepackage[utf8]{inputenc}
\usepackage{color,graphicx}
\usepackage{multirow}
\usepackage{multicol}
%\usepackage{caption} % <- no início do ficheiro !!! Vou comentar porque não funciona no beamer http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=388267
\usepackage{listings}
\usepackage{multimedia}

\lstset{
	numbers=left,
	stepnumber=5,
	firstnumber=1,
	numberstyle=\tiny,
	breaklines=true,
	frame=shadowbox,
	basicstyle=\tiny,
	stringstyle=\ttfamily,
	showstringspaces=false,
	extendedchars=\true,
	inputencoding=utf8,
	tabsize=2,
	%captionpos=bottom
}

% Beamer numbered captions
\setbeamertemplate{caption}[numbered]
\numberwithin{figure}{section}
\numberwithin{table}{section}

%\usetheme{CambridgeUS}
\mode<presentation>{\usetheme{AnnArbor}}
\definecolor{uofsgreen}{rgb}{.125,.5,.25}
%\usecolortheme[named=uofsgreen]{structure}
\setbeamertemplate{footline}[page number]
\AtBeginSection[]
{
\begin{frame}
\tableofcontents[currentsection]
\end{frame}
}

\AtBeginSubsection[]
{
\begin{frame}
\tableofcontents[currentsection,currentsubsection]
\end{frame}
}

\title{\textbf{Introdução à Mineração de Dados}}
\author{Eduardo Ferreira dos Santos}
\institute{Ciência da Computação \\ 
Centro Universitário de Brasília -- UniCEUB}

\date{\today} % Date, can be changed to a custom date
\logo{\includegraphics[scale=0.2]{../imagens/logo-ceub}}

\begin{document}
\begin{frame}
\titlepage
\end{frame}

\begin{frame}
    \frametitle{Professor}
    \begin{center}
        EDUARDO FERREIRA DOS SANTOS, MsC \\
        \bigskip
        Mestre em Computação Aplicada pela Universidade de Brasília -- UnB \\
        \bigskip
        \begin{figure}[ht!]
            \centering
            \includegraphics[scale=0.4]{../imagens/vcard}
        \end{figure}
    \end{center}
    \begin{itemize}
        \item Áreas de pesquisa:
        \begin{itemize}
            \item Processamento de Linguagem Natural:
            \item Recuperação da Informação;
            \begin{itemize}
                \item Modelagem de tópicos;
                \item Busca e recuperação textual;
                \item Classificação de documentos.
            \end{itemize}
            \item Web Semântica;
            \item Bancos de dados NoSQL.
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
	\frame{Sumário}
	\tableofcontents
\end{frame}

\section{Conceitos}

\begin{frame}
    \frametitle{KDD}
    \begin{itemize}
        \item KDD - \emph{Knowledge Discovery on Databases} - é um campo que se preocupa com o ``desenvolvimento de métodos e técnicas para extrair conhecimento dos dados'' \cite{fayyad_data_1996};
        \item Converter dados de baixo nível em formatos mais compactos: \alert{relatórios};
        \item Transformar dados em informação requer \alert{análise manual e interpretação};
        \item KDD também pode ser conhecido como um \alert{método} ou \alert{processo} para \alert{extração de conhecimento em dados}.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Conceituação de KDD}
    \begin{itemize}
        \item Quanto ao resultado \cite{ladeira}:
        \begin{quote}
            Processo, não trivial, de extração de informações, implícitas, previamente \alert{desconhecidas e úteis}, a partir dos dados armazenados em um banco de dados \cite{frawley1991knowledge}
        \end{quote}        
        \item Quanto ao processo:
        \begin{quote}
            Tarefa de descoberta de conhecimento intensivo, consistindo de \alert{interações complexas}, feitas ao longo do tempo, entre o homem e uma \alert{grande base de dados}, possivelmente suportada por um conjunto \alert{heterogêneo de ferramentas} \cite{brachman1995process}
        \end{quote}
        \item KDD lida com \alert{grandes massas de dados};
        \item O conhecimento extraído é \alert{não-trivial}.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{KDD -- Áreas de interação}
    \begin{itemize}
        \item Estatística (\alert{muita estatística});
        \item Reconhecimento de padrões e aprendizagem de máquina;
        \begin{itemize}
            \item Extração de padrões;
            \item Construção de modelos.
        \end{itemize}
        \item Inteligência artificial (conhecimento simbólico)
        \begin{itemize}
            \item Representação;
            \item Interpretação do conhecimento.
        \end{itemize}
        \item Inteligência computacional (conhecimento numérico)
        \begin{itemize}
            \item Aprendizagem;
            \item Generalização.
        \end{itemize}
        \item Bancos de dados (\alert{todos os tipos})
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{KDD -- Definição}
    \begin{definition}
        Processo não trivial de identificação de padrões em um conjunto de dados que possuam as seguintes características \cite{fayyad_data_1996}:
	    \begin{description}
	        \item[validade]	a descoberta de padrões deve ser válida em novos dados
com algum grau de certeza ou probabilidade;
	        \item[novidade] os padrões são novos (pelo menos para o sistema em estudo), ou seja, ainda não foram detectados por nenhuma outra abordagem;
	        \item[utilidade potencial] os padrões devem poder ser utilizados para a tomada de decisões úteis, medidas por alguma função;
	        \item[assimiláveis] um dos objetivos do KDD é tornar os padrões assimiláveis ao conhecimento humano.
	    \end{description}
    \end{definition}
\end{frame}

\begin{frame}
    \frametitle{KD -- Etapas do processo}
    \begin{center}
        O processo KDD é iterativo cíclico e a saída de uma etapa pode requerer uma revisão da etapa anterior. \cite{ladeira}
    \end{center}
    \begin{figure}[ht!]
        \centering
        \includegraphics[width=0.8\textwidth]{../imagens/kdd}
        \label{fig:kdd}
        \caption{Visão geral das etapas que compõem o processo KDD \cite{fayyad_data_1996}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Mineração de dados}
    \begin{itemize}
        \item Algumas definições sobre mineração de dados:
    \end{itemize} 
    \begin{definition}
        \begin{quote}
            Processo de descoberta de \alert{padrões em dados}. O processo deve ser \alert{automático} ou (como é mais comum) semi automático. Os padrões descobertos devem fornecer algum tipo de \alert{vantagem significativa}, como vantagem econômica, por exemplo. Os dados normalmente estão em \alert{grandes quantidades}. \cite{witten-data}
        \end{quote}
    \end{definition}
    \begin{definition}
        \begin{quote}
            Mineração de dados é a \alert{exploração e a análise}, por meio \alert{automático} ou semi-automático, de \alert{grandes quantidades de dados}, a fim de descobrir \alert{padrões e regras significativos}. \cite{berry1997data}
        \end{quote}
    \end{definition}    
\end{frame}

\begin{frame}
    \frametitle{Definição no contexto}
    \begin{definition}
        Mineração de dados é a avaliação de dados eletrônicos com a ajuda de técnicas de aprendizagem para que se possa encontrar relações ou padrões entre eles, visando \cite{ladeira}:
        \begin{itemize}
            \item descobrir novos fatos;
            \item encontrar e descrever padrões;
            \item testar e validar hipóteses.
        \end{itemize}
    \end{definition}
\end{frame}

\begin{frame}
    \frametitle{Descrição das etapas}
    \begin{itemize}
        \item A mineração de dados tem como objetivos \cite{ladeira}:
        \begin{itemize}
            \item \alert{descobrir} novos fatos, regularidades, restrições ou relacionamentos, a
partir da análise dos dados;
            \item encontrar e descrever padrões estruturais (modelos) nos dados, como uma ferramenta que ajuda a \alert{explicar e fazer previsões}:
            \begin{itemize}
                \item Entrada: conjunto de treinamento (envolve algum conceito a ser aprendido);
                \item Saída: modelo (representa forma de predizer novos dados)\footnote{Podem existir muitas descrições alternativas (modelos) que explicam os dados. Em geral, opte pelo \alert{mais simples}}.
            \end{itemize}
            \item \alert{testar} e validar hipóteses (ideias pré-formuladas)
            \begin{itemize}
                \item Entrada: ideias e conjunto de treinamento que permita avaliá-las;
                \item Saída: medida da assertividade do modelo para a hipótese testada.
            \end{itemize}
        \end{itemize}
        \item Os objetivos completam a definição de mineração de dados.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Aprendizagem}
    \begin{itemize}
        \item Como definir o conceito de \alert{aprendizagem}?
        \begin{itemize}
            \item Adição de nova informação à estrutura cognitiva através de um relacionamento com aspecto relevante dessa estrutura\footnote{\url{https://michaelis.uol.com.br/moderno-portugues/busca/portugues-brasileiro/aprendizagem/}};
            \item Qual relacionamento com a estrutura relevante existente um computador é capaz de fazer?
        \end{itemize}
        \pause
        \item Conceito de \alert{treinamento}: construir \alert{relacionamentos} entre os dados e as hipóteses a serem testadas;
        \item Para que se comprove a relação entre causa e efeito (aprendizagem e estrutura existente) é preciso que o treinamento \alert{modifique} de alguma forma a estrutura;
        \item A modificação da estrutura existente gera uma \alert{ressignificação}: novas conclusões a partir do treinamento;
        \item Importante medida: \alert{quanto foi aprendido}?
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Aprendizagem de máquina}
    \begin{itemize}
        \item Para o computador \alert{aprender} é preciso \alert{treinar} para que ele seja capaz de \alert{ressignificar} os dados;
        \item Cada processo de aprendizagem deve fornecer uma medida sobre o \alert{quanto} foi aprendido;
        \item Aprendizagem de máquina está interessada no lado prático sobre o que foi aprendido, e não no teórico;
        \item Dado um conjunto de exemplos, como eu consigo treinar um modelo que seja capaz de fornecer informações em relação aos próximos?
        \begin{itemize}
            \item Qual a melhor lente de contato para prescrever?
            \item Qual a melhor oferta para o cliente no momento da venda?
        \end{itemize}
        \item A medida da aprendizagem se dá em relação à assertividade do modelo nos exemplos fornecidos. 
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Mineração vs aprendizagem de máquina}
    \begin{itemize}
        \item A mineração de dados deseja encontrar \alert{padrões} em dados;
        \item Tais padrões devem facilitar a \alert{tomada de decisão};
        \item Os modelos de aprendizagem de máquina aplicados à mineração de dados devem fornecer insumos para facilitar/automatizar o processo de tomada de decisão com base nos dados;
        \item A comprovação da eficácia/eficiência da decisão se baseia em modelos \alert{estatísticos}.
        \item Em última análise, a mineração de dados se utiliza da junção da estatística, da aprendizagem de máquina e dos bancos de dados.
    \end{itemize}
\end{frame}

\section{Tarefas de mineração de dados}

\begin{frame}
    \frametitle{Objetivos}
    \begin{itemize}
        \item Os principais objetivos de alto nível da mineração de dados são a previsão e a descrição \cite{ladeira}
        \begin{description}
            \item[Previsão] Envolve usar algumas variáveis ou campos da base de dados para prever valores desconhecidos ou futuros de \alert{variáveis de interesse}.
            \item[Descrição] Se concentra em \alert{encontrar padrões} que descrevem os dados, que sejam interpretáveis pelos seres humanos.
        \end{description}
        \item Os objetivos de previsão e descrição são alcançados através da realização das tarefas básicas de mineração.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Exemplo}
    \begin{itemize}
        \item Considere a distribuição correspondente aos atributos renda e dívida de um correntista descrita na Figura \ref{fig:pontos};
        \item Cada pessoa foi classificada como bom pagador (o) ou mau pagador (x).
    \end{itemize}
     \begin{figure}[ht!]
        \centering
        \includegraphics[width=0.6\textwidth]{../imagens/pontos}
        \label{fig:pontos}
        \caption{Dispersão de pontos para bom pagador e mau pagador \cite{ladeira}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Classificação}
    \begin{itemize}
        \item A classificação envolve aprender uma função que mapeia (classifica) um item de dado para uma entre várias classes pré-definidas;
        \item A Figura \ref{fig:classes} apresenta uma superfície de decisão linear:
        \begin{itemize}
            \item Se $w_1 * renda + w_2 * divida < t$ , então o cliente não pega o empréstimo (x)
            \item Possui erro associado.
        \end{itemize}
    \end{itemize}
    \begin{figure}[ht!]
        \centering
        \includegraphics[width=0.55\textwidth]{../imagens/classes}
        \label{fig:classes}
        \caption{Exemplo de classificação \cite{ladeira}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Regressão}
    \begin{itemize}
        \item A regressão consiste em aprender uma função que mapeia um item de dado para uma variável de previsão de valor real;
        \item A Figura \ref{fig:regressao} apresenta um exemplo de regressão para os pontos fornecidos.
    \end{itemize}
    \begin{figure}[ht!]
        \centering
        \includegraphics[width=0.6\textwidth]{../imagens/regressao}
        \label{fig:regressao}
        \caption{Exemplo de regressão \cite{ladeira}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Clustering}
    \begin{itemize}
        \item Clustering é uma tarefa descritiva onde se procura identificar um conjunto finito de categorias ou agrupamentos que descrevem os dados;
        \item A Figura \ref{fig:clusters} apresenta um esboço de formação de cluster para os dados fornecidos.
    \end{itemize}
    \begin{figure}[ht!]
        \centering
        \includegraphics[width=0.6\textwidth]{../imagens/clusters}
        \label{fig:clusters}
        \caption{Exemplo de clustering \cite{ladeira}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Sumarização}
    \begin{itemize}
        \item Problema de descoberta de métodos para encontrar uma descrição compacta para um subconjunto de dados:
        \begin{itemize}
            \item Tabulação da média e desvio padrão de todos os campos;
            \item Análise de relação entre as variáveis.
        \end{itemize}
        \item Métodos mais sofisticados envolvem derivar regras gerais, técnicas de visualização para múltiplas variáveis e a descoberta de relações funcionais entre variáveis:
        \begin{itemize}
            \item Análise exploratória interativa;
            \item Geração automática de relatórios.
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Modelagem de Dependências}
    \begin{itemize}
        \item Encontrar um modelo que descreve dependências significativas entre as variáveis em diferentes níveis
        \begin{description}
            \item[Nível estrutural] Especifica quais variáveis são localmente dependentes entre si;
            \item[Nível quantitativo] Especifica a intensidade da dependência em uma escala numérica.
        \end{description}
        \item Redes probabilísticas são exemplos da modelagem de dependências:
        \begin{itemize}
            \item Redes Bayesianas;
            \item Diagramas de influência;
            \item Naive Bayes;
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Exemplo de dependências}
    \begin{figure}[ht]
        \centering
        \movie[height=0.45\textwidth, width=.7\textwidth,showcontrols]{}{../imagens/asia-animated.mp4}
        \label{fig:asia-animated}
        \caption{Rede Bayesiana conhecida como \emph{Asia Network}\footnote{Disponível em \url{https://www.bayesserver.com/examples/networks/asia}}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Detecção de desvios (Outliers)}
    \begin{itemize}
        \item Descoberta das modificações \alert{mais significativas} em relação aos \alert{níveis históricos}
    \end{itemize}
    \begin{figure}[ht!]
        \centering
        \includegraphics[width=0.5\textwidth]{../imagens/fraud_amount}
        \label{fig:fraud_amount}
        \caption{Exemplo de análise de outliers para detecção de fraudes. Transações fraudulentas tendem a envolver maior quantidade de dinheiro (\emph{cash-out}\footnote{Disponível em \url{https://blog.codecentric.de/en/2017/09/data-science-fraud-detection}})}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Descoberta de associações}
    \begin{itemize}
        \item Problema da \alert{cesta de compras}:
        \begin{itemize}
            \item A cesta possui um grande número de itens;
            \item Clientes adicionam na cesta \alert{subconjuntos} dos itens;
            \item A informação de quais itens foram comprados \alert{juntos} é armazenada.
        \end{itemize}
        \item Regras associativas: $\{X_{1}, X_{2}, ..., X_{n}\}  \Rightarrow Y$
        \begin{itemize}
            \item Caso os itens $X_{1}, X_{2}, ..., X_{n}$ estejam na cesta, então é provável que $Y$ também esteja.
        \end{itemize}
    \end{itemize}
\end{frame}


\section{Modelo CRISP-DM}

\begin{frame}
    \frametitle{Cross Industry Process Model for Data Mining -- CRISP-DM}
    \begin{definition}
        Modelo de processo hierárquico que parte de um conjunto de tarefas mais gerais para um conjunto de tarefas mais específicas, discriminadas em quatro níveis de abstração \cite{ladeira}:
        \begin{enumerate}
            \item no topo da hierarquia, o processo de MD é organizado em \alert{fases};
            \item as fases, por sua vez, são constituídas por diversas \alert{tarefas genéricas}, que formam o segundo nível da hierarquia;
            \item o terceiro nível, de \alert{tarefas especializadas}, envolve a descrição de como as ações das tarefas genéricas são aplicadas em situações específicas;
            \item o quarto nível, de \alert{instâncias do processo}, é um registro das ações, decisões e resultados da mineração de dados de uma aplicação em particular.
        \end{enumerate}
    \end{definition}
\end{frame}


\begin{frame}
    \frametitle{Fases no CRISP-DM}
    \begin{minipage}[ht]{0.59\textwidth}
	    \begin{figure}[ht!]
	        \centering
	        \includegraphics[width=.9\textwidth]{../imagens/crispdm}
	        \label{fig:crispdm}
	        \caption{Diagrama de fases do modelo CRISP-DM \cite{crispdm}}
	    \end{figure}
    \end{minipage}
    \begin{minipage}[ht]{0.39\textwidth}
	    \textbf{Ex.:} Uma tarefa genérica do segundo nível é a limpeza de dados. No terceiro nível, essa tarefa seria descrita em diferentes situações, tais como limpeza de valores numéricos ou de valores categóricos.    
    \end{minipage}
\end{frame}

\subsection{Entendimento do negócio}

\begin{frame}
    \frametitle{Entendimento do negócio}
    \begin{itemize}
        \item Entendimento dos objetivos e requisitos do projeto:
        \begin{itemize}
            \item Ponto de vista do negócio;
            \item Relevância do conhecimento prévio;
            \item Objetivos do usuário final.
        \end{itemize}         
        \item Entregáveis:
        \begin{itemize}
            \item Plano do projeto;
            \item Definição do problema.
        \end{itemize}
    \end{itemize}
\end{frame}

\subsection{Entendimento dos dados}

\begin{frame}
    \frametitle{Entendimento dos dados}
    \begin{itemize}
        \item Seleção do conjunto dos dados;
        \item Análise dos dados:
        \begin{itemize}
            \item Identificar problemas de qualidade;
            \item Levantar dados relevantes para o negócio;
            \item Dicionário de dados:
            \begin{itemize}
                \item Formato;
                \item Quantidade de registros;
                \item Quantidade de campos.
            \end{itemize}
            \item Distribuição dos atributos;
            \item Relacionamento entre pares de atributos;
            \item Identificação de agrupamentos ou subconjuntos existentes nos dados.
        \end{itemize}
    \end{itemize}
\end{frame}

\subsection{Pré-processamento}

\begin{frame}
    \frametitle{Pré-processamento}
    \begin{itemize}
        \item Seleção de atributos, limpeza, construção, integração e formatação dos dados de entrada:
        \begin{itemize}
            \item remoção de ruído ou de dados espúrios;
            \item estratégias para lidar com valores faltantes;
            \item formatação dos dados para a ferramenta utilizada;
            \item criação de atributos derivados e de novos registros;
            \item integração de tabelas;
            \item discretização dos dados numéricos, se necessário.
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Base de dados desbalanceada}
    \begin{itemize}
        \item Desbalanceamento de uma classe (classes raras)
        \begin{itemize}
            \item Casos de uma classe ocorrem com maior frequência que casos de outra(s) classe(s)
            \item Ex.: fraudes são menos frequentes que transações legítimas.
        \end{itemize}
        \item Desbalanceamento de casos dentro de uma classe (casos raros)
        \begin{itemize}
            \item Subconjunto de estados de atributos com menor representação em vista de outros
            \item Ex.: ocorrência de tipos pouco frequentes de fraudes, por exemplo fraudes milionárias.
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Efeitos do desbalanceamento}
    \begin{itemize}
        \item \alert{Desbalanceamento de classes}: classificadores tendem a ignorar a classe minoritária
        \item \alert{Casos raros}: classificadores tendem a ignorar regiões com poucos casos
        \item As duas características são problemáticas quando a classe de interesse é uma classe rara
        \item Modelo tendencioso para a classe majoritária e/ou regiões com mais casos
    \end{itemize}
\end{frame}

\subsection{Modelagem}

\begin{frame}
    \frametitle{Modelagem}
    \begin{itemize}
        \item Quais modelos e parâmetros usaremos?
        \begin{itemize}
            \item função do tipo de dados (numéricos ou nominais);
            \item qual o problema de mineração de dados a resolver.
        \end{itemize}
        \item Elaboração do plano de testes para avaliação dos modelos gerados;
        \item Divisão da massa de dados:
        \begin{itemize}
            \item conjunto de treinamento;
            \item conjunto de testes;
            \item conjunto de validação.
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Seleção de modelos}
    \begin{itemize}
        \item Tarefa de classificação:
        \begin{itemize}
            \item Árvore de decisão;
            \item Classificadores baseados em redes neurais;
            \item Classificadores probabilísticos;
            \item \emph{Deep Learning}.
        \end{itemize}
        \item Tarefa de regressão:
        \begin{itemize}
            \item Regressão linear;
            \item Regressão não linear;
            \item Regressão não linear com redes neurais.
        \end{itemize}
        \item Tarefa de descoberta de associações:
        \begin{itemize}
            \item Modelo neural combinatório;
            \item \emph{Deep Learning}.
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Classificadores -- árvore de decisão}
    \begin{minipage}[ht]{0.48\textwidth}
	    \begin{figure}[ht!]
	        \centering
	        \includegraphics[width=1\textwidth]{../imagens/decision-weather}
	        \label{fig:decision-weather}
	        \caption{Exemplo de árvore de decisão com dados sobre o tempo \cite{witten-data}}
	    \end{figure}    
    \end{minipage}
    \begin{minipage}[ht]{0.48\textwidth}
	    \begin{itemize}
	        \item Vantagens:
	        \begin{itemize}
	            \item Representação compacta e de fácil visualização;
	            \item Decisão complexa decomposta em decisões elementares;
	            \item Fácil detecção de atributos redundantes ou irrelevantes;
	            \item Permite inferir regras de associação.
            \end{itemize}
            \item Desvantagens:
            \begin{itemize}
                \item Pode vir a ser complexa;
                \item Pode não cobrir todos os casos.
            \end{itemize}
	    \end{itemize}    
    \end{minipage}
\end{frame}

\begin{frame}
    \frametitle{Classificadores -- Neurais}
    \begin{minipage}[ht]{0.48\textwidth}
	    \begin{figure}[ht!]
	        \centering
	        \includegraphics[width=1\textwidth]{../imagens/neural-classifier}
	        \label{fig:neural-classifier}
	        \caption{Exemplo de classificador com redes neurais \cite{ladeira}}
	    \end{figure}    
    \end{minipage}
    \begin{minipage}[ht]{0.48\textwidth}
	    \begin{itemize}
	        \item Vantagens:
	        \begin{itemize}
	            \item Usa variáveis categóricas (discretização de valores numéricos);
	            \item Treinamento supervisionado;
	            \item Não requer conhecimento prévio sobre o domínio;
	            \item Facilidade para generalizar e tratar ruídos.
            \end{itemize}
            \item Desvantagens:
            \begin{itemize}
                \item Caixa preta;
                \item Ajuste de parâmetros é artesanal e pode se tornar complexo.
            \end{itemize}
	    \end{itemize}    
    \end{minipage}
\end{frame}

\begin{frame}
    \frametitle{Classificadores -- Naive-Bayes}
    \begin{itemize}
        \item Associa probabilidade à classificação na classes;
        \item Atributos e classes:
        \begin{itemize}
            \item com dependências probabilísticas na forma de um grafo acíclico orientado;
            \item conjunto de treinamento deve ser completo;
            \item valores categóricos. Valores numéricos devem ser discretizados
        \end{itemize}
        \item Considere a seguinte estimativa para as classes da Figura \ref{fig:nb-zeros}:
        \small
	    \[
	        \begin{split}
	            P(China \mid d) \propto & P(China) \cdot P(Beijing \mid China) \cdot P(AND \mid China) \\
	             & \cdot P(TAIPEI \mid China) \cdot P(JOIN \mid China) \cdot {\color{red} P(WTO \mid China)}
	        \end{split}
	    \]
    \end{itemize}
    \begin{figure}[ht]
        \centering
		\includegraphics[width=.7\textwidth]{../imagens/nb-zeros}
		\label{fig:nb-zeros}
		\caption{Exemplo de distribuição de classes \cite{cambridge}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Regressão com redes neurais}
    \begin{minipage}[ht]{0.48\textwidth}
        \begin{figure}[ht]
	        \centering
			\includegraphics[width=1\textwidth]{../imagens/neural-network}
			\label{fig:neural-network}
			\caption{Exemplo de redes neurais com regressão linear \cite{ladeira}}
		\end{figure}
    \end{minipage}
    \begin{minipage}[ht]{0.48\textwidth}
        \begin{itemize}
            \item Vantagens:
            \begin{itemize}
                \item Regressão não linear: não tem conhecimento prévia sobre a forma da função
                \item Conjunto de treinamento completo;
                \item Valores discretos
            \end{itemize}
            \item Desvantagens:
            \begin{itemize}
                \item Caixa preta
                \item Não permite regressão \emph{stepwise}
            \end{itemize}
        \end{itemize}
    \end{minipage}
    \\
    A regressão \emph{stepwise} é um processo que adiciona sistematicamente a variável mais significativa ou remove a variável menos significativa durante cada etapa.
\end{frame}

\subsection{Avaliação}

\begin{frame}
    \frametitle{Avaliação}
    \begin{itemize}
        \item Desempenho no conjunto de treinamento \alert{NÃO} é um bom indicador de desempenho em conjuntos de testes independentes;
        \item Divisão dos dados: treinamento, teste e validação
        \item Classificadores predizem a classe de cada instância:
        \begin{itemize}
            \item Taxa de sucesso: 
            \begin{itemize}
                \item Proporção dos sucessos em relação a todas as instâncias
                \item Qual a relação entre a taxa de sucesso no conjunto de teste
e a verdadeira taxa de sucesso?
                \item Intervalo de confiança para a taxa de sucesso
            \end{itemize}            
        \end{itemize}
        \pause
        \item E a qualidade dos dados?
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Avaliação de classificadores}
    \begin{itemize}
        \item Objetivo: avaliar a qualidade dos modelos escolhidos durante o treinamento:
        \begin{itemize}
            \item Do ponto de vista da análise dos dados;
            \item Critérios \alert{matemáticos} para a seleção dos modelos.
        \end{itemize}
        \item Verificar se os objetivos de negócios foram atingidos
        \item Precision e Recall:
        \begin{itemize}
	        \item Precision(P) é a fração dos documentos recuperados que é relevante
	        \[
	            \begin{split}
		           Precision  &= \frac{\text{\#(classificações corretas)}}{\text{\#(classificações)}} \\
		           Precision &= P(corretos \mid classificados)
	            \end{split}
	        \]
	        \item Recall(R) é a fração dos documentos relevantes que são recuperados
	        \[
	            \begin{split}
		           Recall  &= \frac{\text{\#(classificações corretas)}}{\text{\#(corretas)}} \\
		           Recall &= P(\text{classificados} \mid corretos)
	            \end{split}
	        \]
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Como calcular}
    \begin{definition}
            As medidas Precision $P$ e Recall $R$ são definidas como:
            \[
                \begin{split}
                P &= \frac{TP}{TP + FP} \\
                R &= \frac{TP}{TP + FN}
                \end{split}
            \]
            onde TP e FN são definidos de acordo com a Tabela \ref{tab:precision-recall}.
        \begin{table}[ht]
            \centering
	        \begin{tabular}{c|c|c}
	             & Corretos & Incorretos \\
	             \hline
	             Classificados & True Positives (TP) & Falsos Positives (FP) \\
	             \hline
	             Não classificados & False Negatives (FN) & True Negatives (TN) \\
	        \end{tabular}
	        \label{tab:precision-recall}
	        \caption{Classificadores para positivos e negativos \cite{cambridge}}
	    \end{table}
    \end{definition}
\end{frame}

\begin{frame}
    \frametitle{Avaliação de regressores}
    \begin{itemize}
        \item Maior coeficiente de correlação de Pearson ($r$)
        \item Maior coeficiente de determinação ($r^2$)
        \item Menor erro quadrático médio ($MSE$)
        \item Menor erro absoluto médio
    \end{itemize}
    \begin{figure}[ht]
        \centering
		\includegraphics[width=.7\textwidth]{../imagens/regression-error}
		\label{fig:regression-error}
		\caption{Exemplo de erro em regressão \footnote{\url{https://www.jmp.com/en_us/statistics-knowledge-portal/what-is-regression/interpreting-regression-results.html}}}
	\end{figure}
\end{frame}

\subsection{Deploy}

\begin{frame}
    \frametitle{Deploy}
    \begin{itemize}
        \item Incorporação do modelo selecionado ao processo de tomada de decisão da organização;
        \item Definição de um plano de monitoração e manutenção.
        \begin{itemize}
            \item Previne uso incorreto dos resultados;
            \item Identifique distorções ao longo do tempo.
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Resumo CRISP-DM}
    \begin{figure}[ht]
        \centering
		\includegraphics[width=.8\textwidth]{../imagens/crispdm-process}
		\label{fig:crispdm-process}
		\caption{Resumo do processo CRISP-DM \cite{ladeira}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Observações}
    \begin{itemize}
        \item O CRISP-DM se tornou um padrão \emph{de facto} para conduzir mineração de dados e processos \emph{knowledge discovery} \cite{smu};
        \item Define as entradas e saídas das tarefas;
        \item Desenvolvido pela IBM como \emph{Analytics Solutions Unified Method for Data Mining/Predictive Analytics} -- ASUS-DM;
        \item SAS utiliza SEMMA e a maior parte das empresas de consultoria utiliza seus processos;
        \item Possui muitas áreas em comum com a disciplina emergente de \alert{Ciência de Dados}.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Evolução dos métodos}
    \begin{figure}[ht]
        \centering
		\includegraphics[width=1\textwidth]{../imagens/crispdm-20}
		\label{fig:crispdm-20}
		\caption{CRISP-DM e sua relação com outros métodos \cite{crispdm-20}}
	\end{figure}
\end{frame}

\section{Ciência de dados}

\begin{frame}
    \frametitle{Histórico}
    \begin{minipage}{0.49\textwidth}
	    \begin{figure}[ht]
	        \centering
			\includegraphics[width=1\textwidth]{../imagens/data_mining_timeline}
			\label{fig:data_mining_timeline}
			\caption{Histórico da mineração de dados \cite{smu}}
		\end{figure}
    \end{minipage}
    \begin{minipage}{0.49\textwidth}
	    \begin{itemize}
	        \item A mineração de dados contém, em sua origem, ideias de IA, aprendizagem de máquina, reconhecimento de padrões, estatística e bancos de dados.
	        \item As diferenças se dão em relação ao uso:
	        \begin{itemize}
	            \item Utilização dos dados;
	            \item Objetivos.
	        \end{itemize}
	        \item O grande desafio está no tratamento de \alert{grandes volumes de dados} (\emph{Big Data}).
	    \end{itemize}
    \end{minipage}
\end{frame}

\begin{frame}
    \frametitle{Relacionamento com outras áreas (1)}
    \begin{figure}[ht]
        \centering
		\includegraphics[width=1\textwidth]{../imagens/relationships01}
		\label{fig:relationships01}
		\caption{\cite{smu}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Relacionamento com outras áreas (2)}
    \begin{figure}[ht]
        \centering
		\includegraphics[width=.9\textwidth]{../imagens/relationships02}
		\label{fig:relationships02}
		\caption{\cite{smu}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Relacionamento com outras áreas (3)}
    \begin{figure}[ht]
        \centering
		\includegraphics[width=.9\textwidth]{../imagens/relationships03}
		\label{fig:relationships03}
		\caption{\cite{smu}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Relacionamento com outras áreas (4)}
    \begin{figure}[ht]
        \centering
		\includegraphics[width=.9\textwidth]{../imagens/relationships04}
		\label{fig:relationships04}
		\caption{\cite{smu}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Relacionamento com outras áreas (5)}
    \begin{figure}[ht]
        \centering
		\includegraphics[width=.9\textwidth]{../imagens/relationships05}
		\label{fig:relationships05}
		\caption{\cite{smu}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Relacionamento com outras áreas (6)}
    \begin{figure}[ht]
        \centering
		\includegraphics[width=.9\textwidth]{../imagens/relationships06}
		\label{fig:relationships06}
		\caption{\cite{smu}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Relacionamento com outras áreas (7)}
    \begin{figure}[ht]
        \centering
		\includegraphics[width=.9\textwidth]{../imagens/relationships07}
		\label{fig:relationships07}
		\caption{\cite{smu}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Relacionamento com outras áreas (8)}
    \begin{figure}[ht]
        \centering
		\includegraphics[width=.9\textwidth]{../imagens/relationships08}
		\label{fig:relationships08}
		\caption{\cite{smu}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Mineração de dados e analytics}
    \begin{figure}[ht]
        \centering
		\includegraphics[width=.9\textwidth]{../imagens/md-analytics}
		\label{fig:md-analytics}
		\caption{Relação entre mineração de dados e analytics \cite{smu}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Análise preditiva}
    Quais decisões devem ser tomadas de forma a garantir as maiores receitas? \\
    \bigskip
    \begin{minipage}[ht]{0.49\textwidth}
	    \begin{figure}[ht]
	        \centering
			\includegraphics[width=1\textwidth]{../imagens/prescriptive}
			\label{fig:prescriptive}
			\caption{Exemplo de análise preditiva \cite{smu}}
		\end{figure}    
    \end{minipage}
    \begin{minipage}[ht]{0.49\textwidth}
    \begin{itemize}
        \item Problemas: 
        \begin{itemize}
            \item Quais são as variáveis de decisão? Existe relação de causalidade?
            \item Os relacionamentos podem ser não lineares. A função de decisão converge?
            \item Incerteza sobre qualidade e confiança do modelo preditivo.
        \end{itemize}
    \end{itemize}    
    \end{minipage}
\end{frame}

\begin{frame}
    \frametitle{Cientista de dados}
    Quais as disciplinas necessárias para o profissional cientista de dados? Existe essa pessoa? \\
    \bigskip
    \begin{figure}[ht]
        \centering
		\includegraphics[width=.6\textwidth]{../imagens/data-scientist}
		\label{fig:data-scientist}
		\caption{Disciplinas para o cientista de dados \cite{smu}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Modelos acadêmicos}
    \begin{itemize}
        \item O primeiro Mestrado (2015), posteriormente Doutorado (2017), em Ciência de Dados nos EUA foi oferecido pela NYU\footnote{https://cds.nyu.edu/};
        \item No Mestrado, as disciplinas principais são matemática, ciência da computação e estatística;
        \item No Doutorado, as disciplinas se dividem de acordo com a \alert{área de aplicação}\footnote{Veja as áreas de aplicação em \url{https://cds.nyu.edu/phd-areas-faculty/}}
        \item O programa do UniCEUB é baseado no da NYU: matemática, ciência da computação e estatística.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{No Mercado}
    \begin{itemize}
        \item Na Globo, a carreira se organiza de acordo com a \alert{competência principal} e a senioridade;
        \item Em geral são valorizados os profissionais com diferentes competências (não só na Globo);
        \item Tenha em mente a necessidade de se trabalhar com \alert{grandes volumes de dados}.
    \end{itemize}
    \begin{figure}[ht]
        \centering
		\includegraphics[width=1\textwidth]{../imagens/career-globo}
		\label{fig:career-globo}
		\caption{\emph{Career path} da área de Big Data \& AI da Globo}
	\end{figure}
\end{frame}


\section{Exemplos e aplicações}



\begin{frame}
	\begin{center}
		\textbf{OBRIGADO!!!} \\
		\textbf{PERGUNTAS???}
	\end{center}
\end{frame}

\bibliography{../mineracao-dados}
\bibliographystyle{apalike}

\end{document}
